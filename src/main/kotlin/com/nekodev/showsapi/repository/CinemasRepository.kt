package com.nekodev.showsapi.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Created by Paulina Sadowska on 27.12.2017.
 */
@Repository
interface CinemasRepository : CrudRepository<Cinema, Long> {
    fun findByNameAndCity(name: String, city: String): Iterable<Cinema>
    fun findByCityIgnoreCase(city: String): Iterable<Cinema>
    fun findByLatitudeBetweenAndLongitudeBetween(minLatitude: Double,
                                                 maxLatitude: Double,
                                                 minLongitude: Double,
                                                 maxLongitude: Double): Iterable<Cinema>

    fun findById(id: Long): Iterable<Cinema>
}

@Entity
class Cinema(val name: String,
             val address: String,
             val city: String,
             val latitude: Double,
             val longitude: Double,
             val email: String,
             val phone: String,
             @Id @GeneratedValue(strategy = GenerationType.AUTO)
             val id: Long = -1) {
    constructor() : this("", "", "", 0.0, 0.0, "", "")
    constructor(id: Long) : this("", "", "", 0.0, 0.0, "", "", id)
}
