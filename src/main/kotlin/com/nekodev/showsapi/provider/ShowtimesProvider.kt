package com.nekodev.showsapi.provider

import com.nekodev.showsapi.*
import com.nekodev.showsapi.recommender.RecommenderServiceWrapper
import com.nekodev.showsapi.repository.*
import com.nekodev.showsapi.util.ShowtimeConverter
import org.springframework.stereotype.Component

/**
 * Created by Paulina Sadowska on 22.01.2018.
 */
@Component
class ShowtimesProvider(private val showtimesRepository: ShowtimesRepository,
                        private val cinemasRepository: CinemasRepository,
                        private val usersDataRepository: UsersDataRepository,
                        private val moviesRepository: MoviesRepository,
                        private val recommenderServiceWrapper: RecommenderServiceWrapper) {

    companion object {
        private const val DEFAULT_RATING = 3.0
        private const val NOT_FOUND_MOVIE_ID = -1L
    }

    fun findMoviesPlayedInCinemas(cinemaIds: List<Long>, userToken: String?): List<MovieResponse> {
        return showtimesRepository.findByCinemaIdIn(cinemaIds)
                .map { it -> it.movieId }
                .distinct()
                .sortIfAuthorized(userToken)
                .map { id -> getMovieResponseFromId(id) }
                .filter { it.id >= 0 }
    }

    private fun getMovieResponseFromId(id: Long): MovieResponse {
        val details = moviesRepository.findById(id).firstOrNull()
        return details?.toMovieResponse(MovieDbPosterSize.W342)
                ?: MovieResponse(NOT_FOUND_MOVIE_ID, "", "")
    }

    fun findPopularMovies(): List<MovieResponse> {
        val popularMovies = moviesRepository.findByPopular(true)
        return if (popularMovies.any()) {
            popularMovies
                    .toList()
                    .map { it.toMovieResponse(MovieDbPosterSize.W500) }
        } else {
            throw MoviesNotFoundException()
        }
    }

    fun findShowtimesInCinemasForMovie(movieId: Long, cinemaId: Long, timestampAfter: Long,
                                       timestampBefore: Long?): CinemaWithShowtimesResponse {
        val cinema = cinemasRepository.findById(cinemaId).firstOrNull() ?: throw CinemaNotFoundException(cinemaId)
        val showtimes = getShowtimes(movieId, cinemaId, timestampAfter, timestampBefore)
        return CinemaWithShowtimesResponse(CinemaResponse(cinema), showtimes)
    }

    private fun getShowtimes(movieId: Long, cinemaId: Long, timestampAfter: Long, timestampBefore: Long?): Map<String, List<ShowtimesInDay>> {
        val showtimes = showtimesRepository
                .findByMovieIdAndCinemaIdAndTimestampGreaterThan(movieId, cinemaId, timestampAfter)
                .filter { it -> timestampBefore == null || timestampBefore > it.timestamp }

        return ShowtimeConverter().toShowtimeResponse(showtimes)
    }

    private fun sortMoviesForUser(movieIds: List<Long>, userId: Long): List<Long> {
        return movieIds
                .map { movieId ->
                    try {
                        val ratingResponse = recommenderServiceWrapper.getRating(movieId, userId)
                        if (ratingResponse.isSuccessful) {
                            MovieIdWithRating(movieId, ratingResponse.body()?.rating ?: DEFAULT_RATING)
                        } else {
                            MovieIdWithRating(movieId, DEFAULT_RATING)
                        }
                    } catch (e: Exception) {
                        MovieIdWithRating(movieId, DEFAULT_RATING)
                    }
                }
                .sortedByDescending { movieIdWithRating -> movieIdWithRating.rating }
                .map { it -> it.movieId }
    }

    private fun List<Long>.sortIfAuthorized(userToken: String?): List<Long> {
        return if (userToken != null) {
            val user = usersDataRepository.findByToken(userToken).firstOrNull()
            if (user != null) {
                sortMoviesForUser(this, user.userId)
            } else {
                this
            }
        } else {
            this
        }
    }
}

data class MovieIdWithRating(val movieId: Long,
                             val rating: Double)