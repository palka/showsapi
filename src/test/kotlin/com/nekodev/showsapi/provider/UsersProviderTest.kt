package com.nekodev.showsapi.provider

import com.nekodev.showsapi.IncorrectLoginOrPasswordException
import com.nekodev.showsapi.UserAlreadyExistsException
import com.nekodev.showsapi.repository.UserData
import com.nekodev.showsapi.repository.UserRating
import com.nekodev.showsapi.repository.UsersDataRepository
import com.nekodev.showsapi.repository.UsersRatingRepository
import com.nekodev.showsapi.util.PasswordHasher
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.BDDMockito.anyLong
import org.mockito.BDDMockito.then

/**
 * Created by Paulina Sadowska on 16.06.2018.
 */
class UsersProviderTest {

    private val repository = mock<UsersDataRepository>()
    private val ratingManager = mock<UsersRatingRepository>()
    private val passwordHasher = mock<PasswordHasher>()
    private val usersProvider = UsersProvider(repository, ratingManager, passwordHasher)

    init {
        given(repository.findByLogin(USED_LOGIN)).willReturn((USER_ID..USER_ID).map { UserData(it) })
        given(passwordHasher.getHash(PASSWORD)).willReturn(PASSWORD_HASH)
        given(passwordHasher.creteRandomToken()).willReturn(TOKEN)
    }

    companion object {
        private const val USER_ID = 435L
        private const val PASSWORD_HASH = "some password hash"
        private const val TOKEN = "user token"
        private const val USED_LOGIN = "good login"
        private const val NEW_LOGIN = "new login"
        private const val PASSWORD = "good password"
    }

    @Test(expected = UserAlreadyExistsException::class)
    fun register_userExists_throwsUserAlreadyExistsException() {
        usersProvider.register(USED_LOGIN, PASSWORD)
    }

    @Test
    fun register_userNotExists_returnsAuthTokenResponse() {
        val response = usersProvider.register(NEW_LOGIN, PASSWORD)
        then(repository).should().save(any<UserData>())
        assertEquals(0, response.ratedMoviesCount)
        assertEquals(TOKEN, response.token)
    }

    @Test(expected = IncorrectLoginOrPasswordException::class)
    fun login_userNotExists_throwsIncorrectLoginOrPasswordException() {
        usersProvider.login(NEW_LOGIN, PASSWORD)
    }

    @Test(expected = IncorrectLoginOrPasswordException::class)
    fun login_userExists_correctLoginAndPassword_returnsAuthTokenResponse() {
        given(ratingManager.findByUserId(anyLong())).willReturn((1..10).map { UserRating() })
        val response = usersProvider.login(USED_LOGIN, PASSWORD)
        assertEquals(10, response.ratedMoviesCount)
        assertEquals(TOKEN, response.token)
    }
}