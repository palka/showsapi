package com.nekodev.showsapi.recommender

import org.springframework.stereotype.Component
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response


/**
 * Created by Paulina Sadowska on 28.04.2018.
 */
@Component
class RecommenderServiceWrapper {

    private val recommenderService: RecommenderService

    init {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)

        val recommenderServiceUrl = System.getenv()["RECOMMENDER_SERVICE_URL"] ?: "http://localhost:9000/"
        recommenderService = Retrofit.Builder()
                .baseUrl(recommenderServiceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build()
                .create(RecommenderService::class.java)
    }

    fun getRating(movieId: Long, userId: Long): Response<RatingData> {
        return recommenderService.getRating(movieId, userId).execute()
    }

    fun rateMovie(movieId: Long, userId: Long, ratingBody: RatingBody): Response<String> {
        return recommenderService.rateMovie(movieId, userId, ratingBody).execute()
    }
}