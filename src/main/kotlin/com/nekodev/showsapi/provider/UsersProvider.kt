package com.nekodev.showsapi.provider

import com.nekodev.showsapi.AuthTokenResponse
import com.nekodev.showsapi.IncorrectLoginOrPasswordException
import com.nekodev.showsapi.UserAlreadyExistsException
import com.nekodev.showsapi.UserRatingManager
import com.nekodev.showsapi.repository.RatingsRepository
import com.nekodev.showsapi.repository.UserData
import com.nekodev.showsapi.repository.UsersDataRepository
import com.nekodev.showsapi.repository.UsersRatingRepository
import com.nekodev.showsapi.util.PasswordHasher
import org.springframework.stereotype.Component
import org.springframework.util.DigestUtils
import java.security.SecureRandom


/**
 * Created by Paulina Sadowska on 28.04.2018.
 */
@Component
class UsersProvider(private val usersRepository: UsersDataRepository,
                    private val ratingsManager: UsersRatingRepository,
                    private val passwordHasher: PasswordHasher) {

    fun register(login: String, password: String): AuthTokenResponse {
        if (usersRepository.findByLogin(login).any()) {
            throw UserAlreadyExistsException(login)
        }

        val token = passwordHasher.creteRandomToken()
        val user = UserData(
                login = login,
                passwordHash = passwordHasher.getHash(password),
                token = token)
        usersRepository.save(user)
        return AuthTokenResponse(token, 0)
    }

    fun login(login: String, password: String): AuthTokenResponse {
        val passwordHash = passwordHasher.getHash(password)
        val savedUser = usersRepository.findByLogin(login).firstOrNull()
        if (savedUser != null && passwordHash == savedUser.passwordHash) {
            return AuthTokenResponse(savedUser.token, countUserRatings(savedUser.userId))
        } else {
            throw IncorrectLoginOrPasswordException()
        }
    }

    private fun countUserRatings(userId: Long): Int {
        return ratingsManager.findByUserId(userId).count()
    }
}