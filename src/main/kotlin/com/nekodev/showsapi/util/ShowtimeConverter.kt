package com.nekodev.showsapi.util

import com.nekodev.showsapi.ShowWithLinkResponse
import com.nekodev.showsapi.ShowtimesInDay
import com.nekodev.showsapi.repository.Showtime

/**
 * Created by Paulina Sadowska on 12.05.2018.
 */
class ShowtimeConverter {

    private val timestampFormatter = TimestampFormatter("dd.MM")

    fun toShowtimeResponse(showtimes: List<Showtime>): Map<String, List<ShowtimesInDay>> {
        return showtimes
                .groupBy { timestampFormatter.formatFromSeconds(it.timestamp) }
                .mapValues {
                    getShowsInDay(it.value)
                }
    }

    private fun getShowsInDay(showtimes: List<Showtime>): List<ShowtimesInDay> {
        return showtimes.map {
            val type = ShowType(it)
            val time = ShowWithLinkResponse(it.movieId, it.timestamp, it.bookingPath)
            Pair(type, time)
        }
                .groupBy { it.first.names.joinToString() }
                .map { it -> ShowtimesInDay(it.value.first().first.names, it.value.map { it.second }) }
    }

    class ShowType(showtime: Showtime) {

        companion object {
            private const val _2D = "2D"
            private const val _3D = "3D"
            private const val dubbing = "dubbing"
            private const val subtitles = "subtitles"
        }

        val names: List<String>

        init {
            val mutableNames = mutableListOf<String>()
            if (showtime._2D) {
                mutableNames.add(_2D)
            }

            if (showtime._3D) {
                mutableNames.add(_3D)
            }

            if (showtime.dubbing) {
                mutableNames.add(dubbing)
            }

            if (showtime.subtitles) {
                mutableNames.add(subtitles)
            }

            names = mutableNames.toList()
        }
    }
}

