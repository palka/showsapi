package com.nekodev.showsapi.util

/**
 * Created by Paulina Sadowska on 20.01.2018.
 */
fun Double.toRad(): Double = this * Math.PI / 180.0

fun Double.toDeg(): Double = this * 180 / Math.PI