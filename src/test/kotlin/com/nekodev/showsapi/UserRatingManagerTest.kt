package com.nekodev.showsapi

import com.nekodev.showsapi.recommender.RatingData
import com.nekodev.showsapi.recommender.RecommenderServiceWrapper
import com.nekodev.showsapi.repository.UserData
import com.nekodev.showsapi.repository.UsersDataRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import org.junit.Test
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.mockito.ArgumentMatchers.anyLong
import retrofit2.Response

/**
 * Created by Paulina Sadowska on 16.06.2018.
 */
class UserRatingManagerTest {

    private val repository = mock<UsersDataRepository>()
    private val service = mock<RecommenderServiceWrapper>()
    private val ratingManager = UserRatingManager(repository, service)

    init {
        given(repository.findByToken(TOKEN)).willReturn((USER_ID..USER_ID).map { UserData(it) })
    }

    companion object {
        private const val MOVIE_ID = 1234L
        private const val USER_ID = 435L
        private const val BAD_TOKEN = "bad token"
        private const val TOKEN = "good token"
        private const val RATING = 4.5
    }

    @Test(expected = UnauthorizedAccessException::class)
    fun rateMovie_userNotFound_throwsUnauthorizedAccessException() {
        ratingManager.rateMovie(MOVIE_ID, BAD_TOKEN, RATING)
    }

    @Test(expected = RecommenderServiceUnavailableException::class)
    fun rateMovie_userFound_responseNotSuccess_throwsRecommenderServiceUnavailableException() {
        mockRateResponse(false)
        ratingManager.rateMovie(MOVIE_ID, TOKEN, RATING)
    }

    @Test
    fun rateMovie_userFound_responseSuccess_returnsSuccess() {
        mockRateResponse(true)
        assertEquals("success", ratingManager.rateMovie(MOVIE_ID, TOKEN, RATING))
    }

    @Test(expected = UnauthorizedAccessException::class)
    fun getSavedRating_userNotFound_throwsUnauthorizedAccessException() {
        ratingManager.getSavedRating(MOVIE_ID, BAD_TOKEN)
    }

    @Test(expected = RecommenderServiceUnavailableException::class)
    fun getSavedRating_userFound_responseNotSuccess_throwsRecommenderServiceUnavailableException() {
        mockGetRatingResponse(false)
        ratingManager.getSavedRating(MOVIE_ID, TOKEN)
    }

    @Test
    fun getSavedRating_userFound_responseSuccess_returnsRating() {
        mockGetRatingResponse(true)
        assertEquals(RATING, ratingManager.getSavedRating(MOVIE_ID, TOKEN), 0.01)
    }

    private fun mockRateResponse(success: Boolean) {
        val response = mock<Response<String>>()
        given(response.isSuccessful).willReturn(success)
        given(service.rateMovie(anyLong(), anyLong(), any())).willReturn(response)
    }

    private fun mockGetRatingResponse(success: Boolean) {
        val response = mock<Response<RatingData>>()
        given(response.isSuccessful).willReturn(success)
        given(response.body()).willReturn(RatingData(USER_ID, MOVIE_ID, RATING, false))
        given(service.getRating(anyLong(), anyLong())).willReturn(response)
    }
}