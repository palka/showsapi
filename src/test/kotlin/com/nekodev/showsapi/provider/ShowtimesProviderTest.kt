package com.nekodev.showsapi.provider

import com.nekodev.showsapi.CinemaNotFoundException
import com.nekodev.showsapi.MoviesNotFoundException
import com.nekodev.showsapi.UserAlreadyExistsException
import com.nekodev.showsapi.recommender.RecommenderServiceWrapper
import com.nekodev.showsapi.repository.*
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.mockito.ArgumentMatchers.anyBoolean
import org.mockito.ArgumentMatchers.anyLong

/**
 * Created by Paulina Sadowska on 17.06.2018.
 */
class ShowtimesProviderTest {

    private val showtimesRepository = mock<ShowtimesRepository>()
    private val cinemasRepository = mock<CinemasRepository>()
    private val usersDataRepository = mock<UsersDataRepository>()
    private val moviesRepository = mock<MoviesRepository>()
    private val service = mock<RecommenderServiceWrapper>()

    private val provider = ShowtimesProvider(showtimesRepository, cinemasRepository,
            usersDataRepository, moviesRepository, service)

    companion object {
        private val movies = (1L..10L).map { Movie(it) }
        private const val MOVIE_ID = 666L
        private const val INCORRECT_CINEMA_ID = 1234L
        private const val CINEMA_ID = 12L
    }

    init{
        given(cinemasRepository.findById(INCORRECT_CINEMA_ID)).willReturn(emptyList())
        given(cinemasRepository.findById(CINEMA_ID)).willReturn(listOf(Cinema()))

        given(showtimesRepository
                .findByMovieIdAndCinemaIdAndTimestampGreaterThan(MOVIE_ID, CINEMA_ID, 0))
                .willReturn((1L..10L).map { Showtime(movieId = MOVIE_ID, cinemaId = CINEMA_ID, timestamp = it) })
    }

    @Test(expected = MoviesNotFoundException::class)
    fun findPopularMovies_noMoviesFound_throwsMoviesNotFoundException() {
        given(moviesRepository.findByPopular(anyBoolean())).willReturn(emptyList())
        provider.findPopularMovies()
    }

    @Test
    fun findPopularMovies_moviesFound_returnsPopularMovies() {
        given(moviesRepository.findByPopular(anyBoolean())).willReturn(movies)
        val result = provider.findPopularMovies()
        assertEquals(10, result.size)
        assertEquals(1, result.first().id)
    }

    @Test
    fun findMoviesPlayedInCinemas_noMoviesFound_returnsEmptyList() {
        given(showtimesRepository.findByCinemaIdIn(any())).willReturn(emptyList())
        val result = provider.findMoviesPlayedInCinemas(emptyList(), null)
        assertEquals(0, result.size)
    }

    @Test
    fun findMoviesPlayedInCinemas_distinctMoviesFound_returnsDistinctMoviesList() {
        given(showtimesRepository.findByCinemaIdIn(any())).willReturn((1L..5L).map { Showtime(it) })
        given(moviesRepository.findById(anyLong())).willReturn(movies)
        val result = provider.findMoviesPlayedInCinemas(emptyList(), null)
        assertEquals(5, result.size)
    }

    @Test
    fun findMoviesPlayedInCinemas_nonDistinctMoviesFound_returnsDistinctMoviesList() {
        given(showtimesRepository.findByCinemaIdIn(any())).willReturn(listOf(1L, 2L, 1L, 3L, 2L, 4L).map { Showtime(it) })
        given(moviesRepository.findById(anyLong())).willReturn(movies)
        val result = provider.findMoviesPlayedInCinemas(emptyList(), null)
        assertEquals(4, result.size)
    }

    @Test(expected = CinemaNotFoundException::class)
    fun findShowtimesInCinemasForMovie_noCinemaFound_throwsCinemaNotFoundException() {
        provider.findShowtimesInCinemasForMovie(MOVIE_ID, INCORRECT_CINEMA_ID, 0, null)
    }

    @Test
    fun findShowtimesInCinemasForMovie_noFound_returnsCinemaWithShows() {
        val result = provider.findShowtimesInCinemasForMovie(MOVIE_ID, CINEMA_ID, 0, null)
        assertNotNull(result)
        assertNotNull(result.cinema)
        assertEquals(1, result.showsInDay.size)
        val shows = result.showsInDay.values.first().first().shows
        assertEquals(MOVIE_ID, shows.first().movieId)
        assertEquals(1, shows.first().timestamp)
    }
}