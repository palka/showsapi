package com.nekodev.showsapi.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Created by Paulina Sadowska on 30.12.2017.
 */
@Repository
interface MediaRepository : CrudRepository<Media, Long> {
    fun findByMovieId(movieId: Long): Iterable<Media>
}

@Entity
class Media(val movieId: Long,
            val source: String,
            @Id @GeneratedValue(strategy = GenerationType.AUTO)
            val id: Long = -1) {
    private constructor() : this(0, "")
}