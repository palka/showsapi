package com.nekodev.showsapi.util

/**
 * Created by Paulina Sadowska on 20.01.2018.
 */

import org.springframework.stereotype.Component
import java.lang.*

@Component
class DistanceCalculator {

    fun calculateDistanceInKm(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val theta = lon1 - lon2
        val dist = Math.acos(Math.sin(lat1.toRad()) * Math.sin(lat2.toRad()) + Math.cos(lat1.toRad()) *
                Math.cos(lat2.toRad()) * Math.cos(theta.toRad()))
        return dist.toDeg() * 60.0 * 1.1515 * 1.609344
    }
}
