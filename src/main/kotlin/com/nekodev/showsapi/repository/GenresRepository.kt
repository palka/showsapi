package com.nekodev.showsapi.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Created by Paulina Sadowska on 21.01.2018.
 */
@Repository
interface GenresRepository : CrudRepository<Genre, Long> {
    fun findByMovieId(movieId: Long): Iterable<Genre>
    fun findByMovieIdAndGenreId(movieId: Long, genreId: Long): Iterable<Genre>
}

@Entity
class Genre(val movieId: Long,
            val genreId: Long,
            @Id @GeneratedValue(strategy = GenerationType.AUTO)
            val id: Long = -1) {

    private constructor() : this(0, 0)
}
