package com.nekodev.showsapi.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Created by Paulina Sadowska on 30.12.2017.
 */
@Repository
interface RatingsRepository : CrudRepository<Rating, Long> {
    fun findByMovieId(movieId: Long): Iterable<Rating>
}

@Entity
class Rating(val movieId: Long,
             val filmwebRating: Double = 0.0,
             val imdbRating: Double = 0.0,
             val metacriticRating: Double = 0.0,
             val rottenTomatoesRating: Double = 0.0,
             @Id @GeneratedValue(strategy = GenerationType.AUTO)
             val id: Long = -1) {

    private constructor() : this(0, 0.0, 0.0, 0.0, 0.0)
}

