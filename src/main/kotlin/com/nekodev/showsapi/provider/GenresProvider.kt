package com.nekodev.showsapi.provider

import com.nekodev.showsapi.GenreResponse
import com.nekodev.showsapi.repository.GenreNamesRepository
import com.nekodev.showsapi.repository.GenresRepository
import org.springframework.stereotype.Component

/**
 * Created by Paulina Sadowska on 25.02.2018.
 */

@Component
class GenresProvider(private val genreNamesRepository: GenreNamesRepository,
                     private val genresRepository: GenresRepository) {

    companion object {
        private val POPULAR_GENRES = listOf(
                "akcja", "animacja", "dokumentalny", "dramat", "familijny", "fantasy", "horror",
                "komedia", "krótkometrażowy", "kryminał", "melodramat", "niemy", "przygodowy", "romans", "thriller"
        )
    }

    fun getPopularGenres(): List<GenreResponse> {
        return genreNamesRepository.findAll()
                .map { GenreResponse(it) }
                .filter { POPULAR_GENRES.contains(it.name.toLowerCase()) }
    }

    fun getGenresOfMovie(movieId: Long): List<GenreResponse> {
        return genresRepository.findByMovieId(movieId).flatMap {
            genreNamesRepository.findById(it.genreId).map { GenreResponse(it) }
        }
    }
}