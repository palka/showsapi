package com.nekodev.showsapi

import com.nekodev.showsapi.recommender.RatingBody
import com.nekodev.showsapi.recommender.RatingData
import com.nekodev.showsapi.recommender.RecommenderServiceWrapper
import com.nekodev.showsapi.repository.UsersDataRepository
import org.springframework.stereotype.Component
import retrofit2.Response

/**
 * Created by Paulina Sadowska on 28.04.2018.
 */
@Component
class UserRatingManager(private val usersDataRepository: UsersDataRepository,
                        private val recommenderServiceWrapper: RecommenderServiceWrapper) {

    fun rateMovie(movieId: Long, userToken: String, rating: Double): String {
        with(getUserId(userToken)) {
            try {
                val response = recommenderServiceWrapper
                        .rateMovie(movieId, this, RatingBody(rating))
                if (response.isSuccessful) {
                    return "success"
                }
            } catch (e: Exception) {
            }
            throw RecommenderServiceUnavailableException()
        }
    }

    private fun getUserId(userToken: String): Long {
        return usersDataRepository.findByToken(userToken).firstOrNull()?.userId
                ?: throw UnauthorizedAccessException()
    }

    fun getSavedRating(movieId: Long, userToken: String): Double {
        with(getUserId(userToken)) {
            with(getRatingResponse(movieId, this)?.getBody()) {
                if (this?.isPredicted == false) {
                    return rating
                }

                throw RatingNotFoundException(movieId)
            }
        }
    }

    private fun getRatingResponse(movieId: Long, userId: Long): Response<RatingData>? {
        try {
            return recommenderServiceWrapper.getRating(movieId, userId)
        } catch (e: Exception) {
            throw RecommenderServiceUnavailableException()
        }
    }

    private fun Response<RatingData>.getBody(): RatingData {
        if (isSuccessful) {
            val body = body()
            if (body?.isPredicted == false) {
                return body
            }
        }

        throw RecommenderServiceUnavailableException()
    }
}