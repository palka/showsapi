package com.nekodev.showsapi.util

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Paulina Sadowska on 22.05.2018.
 */
class TimestampFormatter(dateFormat: String, timezone: String = "CET") {

    private val simpleDateFormat: SimpleDateFormat = SimpleDateFormat(dateFormat)

    init {
        val polishTimezone = TimeZone.getTimeZone(timezone)
        simpleDateFormat.timeZone = polishTimezone
    }

    fun formatFromSeconds(timestampSeconds: Long): String {
        return formatFromMilliseconds(timestampSeconds * 1_000)
    }

    fun formatFromMilliseconds(timestampMilliseconds: Long): String {
        return simpleDateFormat.format(timestampMilliseconds)
    }
}