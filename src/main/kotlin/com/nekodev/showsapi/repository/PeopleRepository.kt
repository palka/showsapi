package com.nekodev.showsapi.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Created by Paulina Sadowska on 24.02.2018.
 */
@Repository
interface PeopleRepository : CrudRepository<Person, Long> {
    fun findById(id: Long): Iterable<Person>
    fun findByIdIn(id: List<Long>): Iterable<Person>
    fun findByTheMovieDbId(theMovieDbId: Long): Iterable<Person>
    fun findByName(name: String): Iterable<Person>
}

@Entity
class Person(val name: String,
             val theMovieDbPhotoPath: String,
             val theMovieDbId: Long,
             @Id @GeneratedValue(strategy = GenerationType.AUTO)
             val id: Long = -1) {

    constructor() : this("", "", 0)
}