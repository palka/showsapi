package com.nekodev.showsapi.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Created by Paulina Sadowska on 28.12.2017.
 */
@Repository
interface ShowtimesRepository : CrudRepository<Showtime, Long> {
    fun findByCinemaIdIn(cinemaIds: Collection<Long>): Iterable<Showtime>
    fun findByMovieIdAndCinemaIdAndTimestampGreaterThan(movieId: Long, cinemaId: Long, timestamp: Long): Iterable<Showtime>
}

@Entity
class Showtime(
        val movieId: Long,
        val cinemaId: Long,
        val _2D: Boolean = false,
        val _3D: Boolean = false,
        val dubbing: Boolean = false,
        val subtitles: Boolean = false,
        val timestamp: Long = 0,
        val bookingPath: String = "",
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = -1) {

    private constructor() : this(0)

    constructor(movieId: Long) : this(movieId, 0, false, false, false, false)
}