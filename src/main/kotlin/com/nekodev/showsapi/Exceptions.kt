package com.nekodev.showsapi

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * Created by Paulina Sadowska on 16.01.2018.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
internal class MovieNotFoundException(private val movieId: Long) : RuntimeException() {

    override fun getLocalizedMessage(): String {
        return "movie with id $movieId not found"
    }
}

@ResponseStatus(value = HttpStatus.NOT_FOUND)
internal class MoviesNotFoundException() : RuntimeException() {

    override fun getLocalizedMessage(): String {
        return "movies not found"
    }
}

@ResponseStatus(value = HttpStatus.NOT_FOUND)
internal class PersonNotFoundException(private val personId: Long) : RuntimeException() {

    override fun getLocalizedMessage(): String {
        return "person with id $personId not found"
    }
}

@ResponseStatus(value = HttpStatus.NOT_FOUND)
internal class TrailerNotFoundException(private val movieId: Long) : RuntimeException() {

    override fun getLocalizedMessage(): String {
        return "trailer of movie with id $movieId not found"
    }
}

@ResponseStatus(value = HttpStatus.NOT_FOUND)
internal class CinemaNotFoundException(private val cinemaId: Long) : RuntimeException() {

    override fun getLocalizedMessage(): String {
        return "cinema with id $cinemaId not found"
    }
}

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
internal class UserAlreadyExistsException(private val login: String) : RuntimeException() {

    override fun getLocalizedMessage(): String {
        return "user $login already exists"
    }
}

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
internal class UnauthorizedAccessException : RuntimeException() {

    override fun getLocalizedMessage(): String {
        return "invalid or missing authorization data"
    }
}

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
internal class IncorrectLoginOrPasswordException : RuntimeException() {

    override fun getLocalizedMessage(): String {
        return "incorrect login or password"
    }
}

@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
internal class RecommenderServiceUnavailableException : RuntimeException() {

    override fun getLocalizedMessage(): String {
        return "service unavailable"
    }
}

@ResponseStatus(value = HttpStatus.NOT_FOUND)
internal class RatingNotFoundException(private val movieId: Long) : RuntimeException() {

    override fun getLocalizedMessage(): String {
        return "rating of movie $movieId not found"
    }
}