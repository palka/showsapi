package com.nekodev.showsapi.provider

import com.nekodev.showsapi.CinemaResponse
import com.nekodev.showsapi.repository.CinemasRepository
import com.nekodev.showsapi.util.BoundingBoxCalculator
import com.nekodev.showsapi.util.DistanceCalculator
import org.springframework.stereotype.Component

/**
 * Created by Paulina Sadowska on 31.12.2017.
 */
@Component
class CinemasProvider(private val boundsCalculator: BoundingBoxCalculator,
                      private val cinemasRepository: CinemasRepository,
                      private val distanceCalculator: DistanceCalculator) {


    fun findByCoordinatesAndRadius(longitude: Double, latitude: Double, radiusInKm: Double): List<Long> {
        val bounds = boundsCalculator.calculate(
                longitude = longitude,
                latitude = latitude,
                radiusInKm = radiusInKm)

        val cinemas = cinemasRepository.findByLatitudeBetweenAndLongitudeBetween(
                minLatitude = bounds.minLatitude,
                maxLatitude = bounds.maxLatitude,
                maxLongitude = bounds.maxLongitude,
                minLongitude = bounds.minLongitude
        )

        return cinemas
                .filter { distanceCalculator.calculateDistanceInKm(latitude, longitude, it.latitude, it.longitude) <= radiusInKm }
                .map { it -> it.id }
    }

    fun findByCityName(cityName: String): List<Long> {
        return cinemasRepository.findByCityIgnoreCase(cityName).map { it -> it.id }
    }

    fun getCinemaInfo(cinemaId: Long): CinemaResponse? {
        val cinemas = cinemasRepository.findById(cinemaId)
        return if (cinemas.any())
            CinemaResponse(cinemas.first())
        else null
    }

    fun getAllCinemasInCities(): Map<String, List<Long>> {
        return cinemasRepository.findAll().groupByTo(HashMap(), { it.city }, { it.id })
    }
}