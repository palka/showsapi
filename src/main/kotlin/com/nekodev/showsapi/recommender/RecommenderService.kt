package com.nekodev.showsapi.recommender

import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Paulina Sadowska on 28.04.2018.
 */
interface RecommenderService {

    @POST("/v1/ratings/movies/{movieId}/users/{userId}")
    fun rateMovie(@Path("movieId") movieId: Long,
                  @Path("userId") userId: Long,
                  @Body ratingBody: RatingBody): Call<String>

    @GET("/v1/ratings/movies/{movieId}/users/{userId}")
    fun getRating(@Path("movieId") movieId: Long,
                  @Path("userId") userId: Long): Call<RatingData>
}

data class RatingBody(val rating: Double)

data class RatingData(val userId: Long,
                      val movieId: Long,
                      val rating: Double,
                      val isPredicted: Boolean)

