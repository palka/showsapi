package com.nekodev.showsapi.provider

import com.nekodev.showsapi.MoviesNotFoundException
import com.nekodev.showsapi.recommender.RecommenderServiceWrapper
import com.nekodev.showsapi.repository.*
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.anyLong

/**
 * Created by Paulina Sadowska on 17.06.2018.
 */
class PersonProviderTest {

    private val repository = mock<PeopleRepository>()

    private val provider = PersonProvider(repository)

    @Test
    fun getPerson_noPersonFound_returnsNull() {
        given(repository.findById(anyLong())).willReturn(emptyList())
        assertNull(provider.getPerson(1234))
    }

    @Test
    fun getPerson_personFound_returnsPerson() {
        val personId = 666L
        val personName = "name"
        given(repository.findById(personId)).willReturn(listOf(Person(personName, "", 1L, personId)))
        val result = provider.getPerson(personId)
        assertEquals(personId, result!!.id)
        assertEquals(personName, result.name)
        assertEquals("", result.photoUrl)
    }
}