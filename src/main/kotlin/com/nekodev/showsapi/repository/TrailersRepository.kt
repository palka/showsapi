package com.nekodev.showsapi.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Created by Paulina Sadowska on 01.01.2018.
 */

@Repository
interface TrailersRepository : CrudRepository<Trailer, Long> {
    fun findByMovieId(movieId: Long): Iterable<Trailer>
}

@Entity
class Trailer(
        val movieId: Long,
        val youtubeId: String,
        val title: String = "",
        val thumbnail: String,
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = -1) {
    private constructor() : this(0, "", "", "")
}