package com.nekodev.showsapi.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Created by Paulina Sadowska on 28.04.2018.
 */
@Repository
interface UsersDataRepository : CrudRepository<UserData, Long> {
    fun findByUserId(userId: Long): Iterable<UserData>
    fun findByToken(token: String): Iterable<UserData>
    fun findByLogin(login: String): Iterable<UserData>
}

@Entity
class UserData(val login: String,
               val passwordHash: String,
               val token: String,
               @Id @GeneratedValue(strategy = GenerationType.AUTO)
               val userId: Long = -1) {
    private constructor() : this("", "", "")
    constructor(id: Long) : this("", "", "", id)
}