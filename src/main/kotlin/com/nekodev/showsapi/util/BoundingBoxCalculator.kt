package com.nekodev.showsapi.util

import org.springframework.stereotype.Component

/**
 * Created by Paulina Sadowska on 31.12.2017.
 */
@Component
class BoundingBoxCalculator {

    private val EARTH_RADIUS_KM = 6371  // earth's mean radius, km

    fun calculate(latitude: Double, longitude: Double, radiusInKm: Double): BoundingBox {

        val radiusToEarthRadius = radiusInKm / EARTH_RADIUS_KM
        val latitudeDifference = radiusToEarthRadius.toDeg()
        val longitudeDifference = (Math.asin(radiusToEarthRadius) / Math.cos(latitude.toRad())).toDeg()

        val minLatitude = latitude - latitudeDifference
        val maxLatitude = latitude + latitudeDifference
        val minLongitude = longitude - longitudeDifference
        val maxLongitude = longitude + longitudeDifference

        return BoundingBox(
                minLatitude = minLatitude,
                maxLatitude = maxLatitude,
                minLongitude = minLongitude,
                maxLongitude = maxLongitude
        )
    }
}

data class BoundingBox(val minLatitude: Double,
                       val maxLatitude: Double,
                       val minLongitude: Double,
                       val maxLongitude: Double)