package com.nekodev.showsapi.provider

import com.nekodev.showsapi.repository.Cinema
import com.nekodev.showsapi.repository.CinemasRepository
import com.nekodev.showsapi.util.BoundingBox
import com.nekodev.showsapi.util.BoundingBoxCalculator
import com.nekodev.showsapi.util.DistanceCalculator
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.ArgumentMatchers.*

/**
 * Created by Paulina Sadowska on 17.06.2018.
 */
class CinemasProviderTest {

    private val boundsCalculator: BoundingBoxCalculator
    private val cinemasRepository: CinemasRepository
    private val distanceCalculator: DistanceCalculator
    private val provider: CinemasProvider

    init {
        boundsCalculator = mock {
            on { calculate(anyDouble(), anyDouble(), anyDouble()) } doReturn BoundingBox(0.0, 0.0, 0.0, 0.0)
        }
        cinemasRepository = mock {
            on { findByLatitudeBetweenAndLongitudeBetween(anyDouble(), anyDouble(), anyDouble(), anyDouble()) } doReturn listOf(Cinema())
        }
        distanceCalculator = mock()
        provider = CinemasProvider(boundsCalculator, cinemasRepository, distanceCalculator)
    }

    @Test
    fun findByCoordinatesAndRadius_noCinemasFound_returnEmptyList() {
        given(cinemasRepository.findByLatitudeBetweenAndLongitudeBetween(anyDouble(), anyDouble(), anyDouble(), anyDouble()))
                .willReturn(emptyList())
        val result = provider.findByCoordinatesAndRadius(0.0, 0.0, 20.0)
        assertTrue(result.isEmpty())
    }

    @Test
    fun findByCoordinatesAndRadius_cinemaFound_outsideRadius_returnEmptyList() {
        mockDistance(30.0)
        val result = provider.findByCoordinatesAndRadius(0.0, 0.0, 20.0)
        assertTrue(result.isEmpty())
    }

    @Test
    fun findByCoordinatesAndRadius_cinemaFound_insideRadius_returnCinemasList() {
        mockDistance(10.0)
        val result = provider.findByCoordinatesAndRadius(0.0, 0.0, 20.0)
        assertEquals(1, result.size)
    }

    @Test
    fun findByCityName_noCinemaFound_returnEmptyList() {
        given(cinemasRepository.findByCityIgnoreCase(anyString())).willReturn(emptyList())
        val result = provider.findByCityName("")
        assertTrue(result.isEmpty())
    }

    @Test
    fun findByCityName_cinemaFound_returnCinemasIdList() {
        given(cinemasRepository.findByCityIgnoreCase(anyString())).willReturn(listOf(Cinema(1L), Cinema(2L)))
        val result = provider.findByCityName("")
        assertEquals(2, result.size)
    }

    private fun mockDistance(distance: Double) {
        given(distanceCalculator.calculateDistanceInKm(anyDouble(), anyDouble(), anyDouble(), anyDouble())).willReturn(distance)
    }
}