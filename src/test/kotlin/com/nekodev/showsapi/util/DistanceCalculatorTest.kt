package com.nekodev.showsapi.util

import org.junit.Assert
import org.junit.Test

/**
 * Created by Paulina Sadowska on 16.06.2018.
 */
class DistanceCalculatorTest {

    companion object {
        private const val LNG_POZNAN = 16.9481105
        private const val LAT_POZNAN = 52.4021343

        private const val LNG_WARSAW = 21.000704
        private const val LAT_WARSAW = 52.2123189

        private const val EXPECTED_DISTANCE = 276.28
    }

    @Test
    fun calculateDistanceInKm_fromPoznanToWarsaw_returnsDistanse() {
        val distanceCalculator = DistanceCalculator()
        val result = distanceCalculator.calculateDistanceInKm(LAT_POZNAN, LNG_POZNAN, LAT_WARSAW, LNG_WARSAW);
        Assert.assertEquals(EXPECTED_DISTANCE, result, 0.01)
    }

}