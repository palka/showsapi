package com.nekodev.showsapi.provider

import com.nekodev.showsapi.PersonResponse
import com.nekodev.showsapi.repository.PeopleRepository
import com.nekodev.showsapi.util.TheMovieDbImageUrlCreator
import org.springframework.stereotype.Component

/**
 * Created by Paulina Sadowska on 25.02.2018.
 */
@Component
class PersonProvider(private val peopleRepository: PeopleRepository) {

    private val imageUlrCreator = TheMovieDbImageUrlCreator()

    fun getPerson(personId: Long): PersonResponse? {
        val foundPeople = peopleRepository.findById(personId)
        return if (foundPeople.any()) {
            val person = foundPeople.first()
            PersonResponse(id = person.id,
                    name = person.name,
                    photoUrl = getImageUrl(person.theMovieDbPhotoPath))
        } else null
    }

    private fun getImageUrl(imagePath: String) = imageUlrCreator.getProfileImageUrl(imagePath) ?: ""
}