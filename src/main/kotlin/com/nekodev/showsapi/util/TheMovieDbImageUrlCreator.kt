package com.nekodev.showsapi.util

/**
 * Created by Paulina Sadowska on 25.02.2018.
 */
class TheMovieDbImageUrlCreator {

    companion object {
        const val BASE_IMAGE_URL = "http://image.tmdb.org/t/p/"
    }

    fun getProfileImageUrl(path: String, size: ProfileImageSize = ProfileImageSize.W185) = getImageUrl(path = path, size = size.value)

    fun getPosterUrl(path: String, size: PosterSize = PosterSize.W342) = getImageUrl(path = path, size = size.value)

    fun getBackdropUrl(path: String, size: BackdropSize = BackdropSize.W780) = getImageUrl(path = path, size = size.value)

    private fun getImageUrl(path: String, size: String): String? {
        return if (path.isNotBlank()) {
            "$BASE_IMAGE_URL$size/$path"
        } else {
            null
        }
    }
}

enum class BackdropSize(val value: String) {
    W300("w300"),
    W780("w780"),
    W1280("w1280"),
    ORIGINAL("original")
}

enum class PosterSize(val value: String) {
    W92("w92"),
    W154("w154"),
    W185("w185"),
    W342("w342"),
    W500("w500"),
    W700("w780"),
    ORIGINAL("original")
}

enum class ProfileImageSize(val value: String) {
    W45("w45"),
    W185("w185"),
    W632("w632"),
    ORIGINAL("original")
}