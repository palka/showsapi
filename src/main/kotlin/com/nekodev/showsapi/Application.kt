package com.nekodev.showsapi

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

/**
 * Created by Paulina Sadowska on 31.12.2017.
 */
@SpringBootApplication
@ComponentScan("com.nekodev.showsapi")
open class Application


fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}