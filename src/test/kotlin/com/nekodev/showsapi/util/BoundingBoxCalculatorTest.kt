package com.nekodev.showsapi.util

import org.junit.Test

import org.junit.Assert.*
import java.lang.Math.sqrt

/**
 * Created by Paulina Sadowska on 31.12.2017.
 */
class BoundingBoxCalculatorTest {

    private val minAccuracy = 0.05
    private val calculator: BoundingBoxCalculator = BoundingBoxCalculator()
    private val distanceCalculator: DistanceCalculator = DistanceCalculator()

    @Test
    fun calculate_radiusInteger() {
        val radiusInKm = 10.0
        val box = calculator.calculate(32.9697, -96.80322, radiusInKm)
        checkBoxHasRightSize(box, radiusInKm)
    }

    @Test
    fun calculate_radiusDecimal() {
        val radiusInKm = 1.872
        val box = calculator.calculate(22.0234, 26.89322, radiusInKm)
        checkBoxHasRightSize(box, radiusInKm)
    }

    private fun checkBoxHasRightSize(box: BoundingBox, radiusInKm: Double) {
        val side1 = distanceCalculator.calculateDistanceInKm(box.minLatitude, box.minLongitude, box.maxLatitude, box.minLongitude)
        val side2 = distanceCalculator.calculateDistanceInKm(box.minLatitude, box.maxLongitude, box.maxLatitude, box.maxLongitude)
        val side3 = distanceCalculator.calculateDistanceInKm(box.maxLatitude, box.minLongitude, box.maxLatitude, box.maxLongitude)
        val side4 = distanceCalculator.calculateDistanceInKm(box.minLatitude, box.minLongitude, box.minLatitude, box.maxLongitude)
        val diagonal = distanceCalculator.calculateDistanceInKm(box.minLatitude, box.minLongitude, box.maxLatitude, box.maxLongitude)
        
        assertEquals(radiusInKm * 2, side1, minAccuracy)
        assertEquals(radiusInKm * 2, side2, minAccuracy)
        assertEquals(radiusInKm * 2, side3, minAccuracy)
        assertEquals(radiusInKm * 2, side4, minAccuracy)
        assertEquals(radiusInKm * 2 * sqrt(2.0), diagonal, minAccuracy)
    }
}