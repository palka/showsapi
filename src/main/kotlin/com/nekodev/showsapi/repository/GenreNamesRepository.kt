package com.nekodev.showsapi.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Created by Paulina Sadowska on 21.01.2018.
 */
@Repository
interface GenreNamesRepository : CrudRepository<GenreName, Long> {
    fun findById(id: Long): Iterable<GenreName>
    fun findByIdIn(id: List<Long>): Iterable<GenreName>
    fun findByName(name: String): Iterable<GenreName>
}

@Entity
class GenreName(val name: String,
                @Id @GeneratedValue(strategy = GenerationType.AUTO)
                val id: Long = -1) {

    private constructor() : this("")
}
