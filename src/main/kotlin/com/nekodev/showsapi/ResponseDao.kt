@file:Suppress("MemberVisibilityCan", "MemberVisibilityCanBePrivate")

package com.nekodev.showsapi

import com.nekodev.showsapi.repository.*

/**
 * Created by Paulina Sadowska on 16.01.2018.
 */
data class MovieResponse(val id: Long,
                         val title: String,
                         val posterUrl: String)

fun Movie.toMovieResponse(width: MovieDbPosterSize): MovieResponse {
    val posterUrl = if (theMovieDbPhotoPath.isNotBlank()) {
        "https://image.tmdb.org/t/p/${width.size}/${theMovieDbPhotoPath}"
    } else {
        posterUrl
    }

    return MovieResponse(id, title, posterUrl)
}

enum class MovieDbPosterSize(val size: String) {
    W92("w92"),
    W154("w154"),
    W185("w185"),
    W342("w342"),
    W500("w500"),
    W780("w780"),
    ORIGINAL("original")
}

data class MovieDetailsResponse(val id: Long,
                                val title: String,
                                val originalTitle: String,
                                val description: String,
                                val length: Int,
                                val ageLimit: String,
                                val releaseDateTimestamp: Long,
                                val director: String,
                                val crew: List<String>,
                                val genres: List<String>,
                                val posterUrl: String,
                                val backdropUrl: String?) {

    constructor(movie: Movie, posterUrl: String, backdropUrl: String?, cast: List<String>, genres: List<String>) : this(
            id = movie.id,
            title = movie.title,
            originalTitle = movie.originalTitle,
            description = movie.description,
            length = movie.length,
            ageLimit = movie.ageLimit,
            releaseDateTimestamp = movie.releaseDate,
            director = movie.director,
            crew = cast,
            genres = genres,
            posterUrl = posterUrl,
            backdropUrl = backdropUrl
    )
}

data class TrailerResponse(val movieId: Long,
                           val youtubeId: String,
                           val title: String,
                           val thumbnail: String) {
    constructor(trailer: Trailer) : this(
            movieId = trailer.movieId,
            youtubeId = trailer.youtubeId,
            title = trailer.title,
            thumbnail = trailer.thumbnail
    )
}

data class RatingResponse(val movieId: Long,
                          val filmwebRating: Double,
                          val imdbRating: Double,
                          val metacriticRating: Double,
                          val rottenTomatoesRating: Double) {
    constructor(rating: Rating) : this(
            movieId = rating.movieId,
            filmwebRating = rating.filmwebRating,
            imdbRating = rating.imdbRating,
            metacriticRating = rating.metacriticRating,
            rottenTomatoesRating = rating.rottenTomatoesRating
    )
}

data class CinemaWithShowtimesResponse(val cinema: CinemaResponse,
                                       val showsInDay: Map<String, List<ShowtimesInDay>>)

data class ShowtimesInDay(val types: List<String>,
                          val shows: List<ShowWithLinkResponse>)

data class ShowWithLinkResponse(val movieId: Long,
                                val timestamp: Long,
                                val bookingPath: String)

data class CinemaResponse(var name: String,
                          var address: String,
                          var city: String,
                          var latitude: Double,
                          var longitude: Double,
                          var email: String,
                          var phone: String) {
    constructor(cinema: Cinema) : this(
            name = cinema.name,
            address = cinema.address,
            city = cinema.city,
            latitude = cinema.latitude,
            longitude = cinema.longitude,
            email = cinema.email,
            phone = cinema.phone
    )
}

data class GenreResponse(var id: Long,
                         var name: String) {
    constructor(genre: GenreName) : this(
            id = genre.id,
            name = genre.name)
}

data class PersonResponse(var id: Long,
                          var name: String,
                          var photoUrl: String)

data class AuthTokenResponse(var token: String,
                             var ratedMoviesCount: Int)