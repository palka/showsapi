package com.nekodev.showsapi.controller

import com.nekodev.showsapi.MovieNotFoundException
import com.nekodev.showsapi.TrailerNotFoundException
import com.nekodev.showsapi.provider.UsersProvider
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

/**
 * Created by Paulina Sadowska on 28.04.2018.
 */
@RestController
class UsersController(private val usersProvider: UsersProvider) {

    @PostMapping("/login")
    fun login(@RequestParam(value = "login", required = true) login: String,
              @RequestParam(value = "password", required = true) password: String) =
            usersProvider.login(login, password)

    @PostMapping("/register")
    fun register(@RequestParam(value = "login", required = true) login: String,
                 @RequestParam(value = "password", required = true) password: String) =
            usersProvider.register(login, password)

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(MovieNotFoundException::class, TrailerNotFoundException::class)
    fun handleError(exception: Exception): String {
        return exception.localizedMessage
    }
}