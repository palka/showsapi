package com.nekodev.showsapi.repository

import org.hibernate.annotations.Table
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.io.Serializable
import javax.persistence.Entity
import javax.persistence.Id

/**
 * Created by Paulina Sadowska on 27.05.2018.
 */
@Repository
interface UsersRatingRepository : CrudRepository<UserRating, Long> {
    fun findByUserId(userId: Long): Iterable<UserRating>
}

@Entity
class UserRating(val movieId: Long,
                 @Id val userId: Long,
                 val rating: Double) {
    constructor() : this(0, 0, 0.0)
}