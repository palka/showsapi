package com.nekodev.showsapi.provider

import com.nekodev.showsapi.MovieDetailsResponse
import com.nekodev.showsapi.RatingResponse
import com.nekodev.showsapi.TrailerResponse
import com.nekodev.showsapi.repository.*
import com.nekodev.showsapi.util.TheMovieDbImageUrlCreator
import org.springframework.stereotype.Component

/**
 * Created by Paulina Sadowska on 01.01.2018.
 */
@Component
class MoviesProvider(private val moviesRepository: MoviesRepository,
                     private val mediaRepository: MediaRepository,
                     private val peopleRepository: PeopleRepository,
                     private val genresRepository: GenresRepository,
                     private val genreNamesRepository: GenreNamesRepository,
                     private val trailersRepository: TrailersRepository,
                     private val ratingsRepository: RatingsRepository) {

    companion object {
        private const val LIST_ITEMS_SEPARATOR = ";"
        private const val NOT_NUMBER_REGEX_PATTERN = "^[0-9]+\$"
    }

    private val imageUlrCreator = TheMovieDbImageUrlCreator()


    fun getMovieDetails(movieId: Long): MovieDetailsResponse? {
        val foundMovies = moviesRepository.findById(movieId)
        return if (foundMovies.any()) {
            val movie = foundMovies.first()
            val castIds = getCastIdsList(movie.castIds)
            val cast = getCastByIds(castIds)
            val genres = getGenreNames(movieId)
            MovieDetailsResponse(movie, getPosterPath(movie), getBackdropPath(movie.theMovieDbBackdropImagePath), cast, genres)
        } else null
    }

    private fun getCastIdsList(castIds: String): List<Long> {
        return castIds.split(LIST_ITEMS_SEPARATOR)
                .filter { it.isNotBlank() && it.contains(NOT_NUMBER_REGEX_PATTERN.toRegex()) }
                .map { it.toLong() }
    }

    private fun getCastByIds(castIds: List<Long>): List<String> {
        return peopleRepository.findByIdIn(castIds)
                .map { it.name }
    }

    private fun getGenreNames(movieId: Long): List<String> {
        val genreIds =  genresRepository.findByMovieId(movieId).map { it.genreId }
        return genreNamesRepository.findByIdIn(genreIds).map { it.name }
    }

    private fun getPosterPath(movie: Movie): String {
        return imageUlrCreator.getPosterUrl(movie.theMovieDbPhotoPath) ?: movie.posterUrl
    }

    private fun getBackdropPath(imagePath: String) = imageUlrCreator.getBackdropUrl(imagePath)

    fun getMoviePhotos(movieId: Long): List<String> =
            mediaRepository.findByMovieId(movieId).map { it -> it.source }

    fun getMovieTrailer(movieId: Long): TrailerResponse? {
        val foundTrailers = trailersRepository.findByMovieId(movieId)
        return if (foundTrailers.any())
            TrailerResponse(foundTrailers.first())
        else null
    }

    fun getMovieRatings(movieId: Long): RatingResponse? {
        val foundRatings = ratingsRepository.findByMovieId(movieId)
        return if (foundRatings.any())
            RatingResponse(foundRatings.first())
        else null
    }
}