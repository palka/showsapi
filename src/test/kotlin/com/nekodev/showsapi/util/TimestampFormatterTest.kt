package com.nekodev.showsapi.util

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Paulina Sadowska on 22.05.2018.
 */

class TimestampFormatterTest {

    companion object {
        private const val TIMESTAMP_22_05_22_30_UTC = 1527028200L
        private const val DATE_FORMAT = "dd.MM HH:mm"
    }

    @Test
    fun formatDate_CET() {
        val formatter = TimestampFormatter(DATE_FORMAT, "CET")
        val result = formatter.formatFromSeconds(TIMESTAMP_22_05_22_30_UTC)
        assertEquals("23.05 00:30", result)
    }

    @Test
    fun formatDate_UTC() {
        val formatter = TimestampFormatter(DATE_FORMAT, "UTC")
        val result = formatter.formatFromSeconds(TIMESTAMP_22_05_22_30_UTC)
        assertEquals("22.05 22:30", result)
    }
}