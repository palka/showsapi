package com.nekodev.showsapi.util


import org.springframework.stereotype.Component
import org.springframework.util.DigestUtils
import java.security.SecureRandom

/**
 * Created by Paulina Sadowska on 16.06.2018.
 */
@Component
class PasswordHasher {

    fun getHash(password: String): String {
        return DigestUtils.md5DigestAsHex(password.toByteArray())
    }

    fun creteRandomToken(): String {
        val bytes = ByteArray(20)
        SecureRandom().nextBytes(bytes)
        return getHash(bytes.toString())
    }
}