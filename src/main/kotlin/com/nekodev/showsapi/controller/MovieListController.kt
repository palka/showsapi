package com.nekodev.showsapi.controller

import com.nekodev.showsapi.*
import com.nekodev.showsapi.provider.*
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

/**
 * Created by Paulina Sadowska on 31.12.2017.
 */
@RestController
class MovieListController(private val cinemasProvider: CinemasProvider,
                          private val moviesProvider: MoviesProvider,
                          private val showtimesProvider: ShowtimesProvider,
                          private val genresProvider: GenresProvider,
                          private val personProvider: PersonProvider,
                          private val userRatingManager: UserRatingManager) {

    @GetMapping("/cinemasInCities")
    fun findAllCities() =
            cinemasProvider.getAllCinemasInCities()

    @GetMapping("/cinemas")
    fun findCinemasByLocation(@RequestParam(value = "latitude", required = true) latitude: Double,
                              @RequestParam(value = "longitude", required = true) longitude: Double,
                              @RequestParam(value = "radiusInKm", required = true) radiusInKm: Double) =
            cinemasProvider.findByCoordinatesAndRadius(
                    longitude = longitude,
                    latitude = latitude,
                    radiusInKm = radiusInKm)

    @GetMapping("/cinemas/{cityName}")
    fun findCinemasByCityName(@PathVariable cityName: String) =
            cinemasProvider.findByCityName(cityName)

    @GetMapping("/cinemas/{cinemaId}/details")
    fun getCinemaDetails(@PathVariable cinemaId: Long) =
            cinemasProvider.getCinemaInfo(cinemaId) ?: throw CinemaNotFoundException(cinemaId)

    @GetMapping("/movies")
    fun findMoviesInCinemas(@RequestParam(value = "cinema", required = true) cinemaIds: List<Long>,
                            @RequestParam(value = "token", required = false) userToken: String?) =
            showtimesProvider.findMoviesPlayedInCinemas(cinemaIds, userToken)

    @GetMapping("/movies/popular")
    fun findPopularMovies() = showtimesProvider.findPopularMovies()

    @GetMapping("/movies/{movieId}/showtimes")
    fun findShowtimesOfMovie(@PathVariable movieId: Long,
                             @RequestParam(value = "cinema", required = true) cinemaId: Long,
                             @RequestParam(value = "timestampAfter", required = true) timestampAfter: Long,
                             @RequestParam(value = "timestampBefore", required = false) timestampBefore: Long?) =
            showtimesProvider.findShowtimesInCinemasForMovie(movieId, cinemaId, timestampAfter, timestampBefore)

    @GetMapping("/movies/{movieId}")
    fun getMovieDetails(@PathVariable movieId: Long): MovieDetailsResponse {
        return moviesProvider.getMovieDetails(movieId) ?: throw MovieNotFoundException(movieId)
    }

    @GetMapping("/movies/{movieId}/photos")
    fun getMoviePhotos(@PathVariable movieId: Long): List<String> {
        return moviesProvider.getMoviePhotos(movieId)
    }

    @GetMapping("/movies/{movieId}/trailer")
    fun getMovieTrailer(@PathVariable movieId: Long): TrailerResponse {
        return moviesProvider.getMovieTrailer(movieId) ?: throw TrailerNotFoundException(movieId)
    }

    @GetMapping("/movies/{movieId}/ratings")
    fun getMovieRatings(@PathVariable movieId: Long): RatingResponse {
        return moviesProvider.getMovieRatings(movieId) ?: throw MovieNotFoundException(movieId)
    }

    @GetMapping("/genres/popular")
    fun findAllGenres() =
            genresProvider.getPopularGenres()

    @GetMapping("/movies/{movieId}/genres")
    fun findGenresOfMovie(@PathVariable movieId: Long) =
            genresProvider.getGenresOfMovie(movieId)

    @PostMapping("/movies/{movieId}/user/rating")
    fun setMovieRatings(@PathVariable movieId: Long,
                        @RequestParam(value = "token", required = true) userToken: String,
                        @RequestParam(value = "rating", required = true) rating: Double): String {
        return userRatingManager.rateMovie(movieId, userToken, rating)
    }

    @GetMapping("/movies/{movieId}/user/rating")
    fun getMovieRatings(@PathVariable movieId: Long,
                        @RequestParam(value = "token", required = true) userToken: String): Double {
        return userRatingManager.getSavedRating(movieId, userToken)
    }


    @GetMapping("/people/{personId}")
    fun getPerson(@PathVariable personId: Long): PersonResponse {
        return personProvider.getPerson(personId) ?: throw PersonNotFoundException(personId)
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(MovieNotFoundException::class, TrailerNotFoundException::class)
    fun handleError(exception: Exception): String {
        return exception.localizedMessage
    }
}