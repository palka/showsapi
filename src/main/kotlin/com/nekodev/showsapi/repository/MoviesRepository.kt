package com.nekodev.showsapi.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.persistence.*

/**
 * Created by Paulina Sadowska on 26.12.2017.
 */
@Repository
interface MoviesRepository : CrudRepository<Movie, Long> {
    fun findByScrappedTitle(scrappedTitle: String): Iterable<Movie>
    fun findById(id: Long): Iterable<Movie>
    fun findByPopular(popular: Boolean): Iterable<Movie>
}

//THE MOVIE DB IMAGE PATHS
//"w92", "w154", "w185", "w342", "w500", "w780", or "original"
//http://image.tmdb.org/t/p/w342/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg

@Entity
class Movie(val scrappedTitle: String,
            val title: String,
            val originalTitle: String,
            @Column(length = 2048)
            val description: String,
            val length: Int,
            val ageLimit: String,
            val releaseYear: Int,
            val releaseDate: Long,
            val director: String,
            val castIds: String,
            val posterUrl: String,
            val theMovieDbPhotoPath: String,
            val theMovieDbBackdropImagePath: String,
            val popular: Boolean,
            @Id @GeneratedValue(strategy = GenerationType.AUTO)
            val id: Long = -1) {

    constructor() : this("", "", "", "", 0, "", 0, 0, "",
            "", "", "", "", false)

    constructor(id: Long) : this("", "", "", "", 0, "", 0, 0, "",
            "", "", "", "", false, id)
}

